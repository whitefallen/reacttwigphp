<?php
require __DIR__ . '/vendor/autoload.php';

use \Twig\Environment;
use \Twig\Loader\FilesystemLoader;

$loader = new FilesystemLoader("templates");
$twig = new Environment($loader);

echo $twig->render('base.html.twig');
