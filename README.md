# React & Twig & PHP

## Requirements
    - have Node.js installed
    - have PHP >= 7.1
    - have Yarn installed
    - have Composer installed

## Installation
    - clone the repo
    - composer update
    - npm install
    - yarn encore dev --watch